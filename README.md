# Fediverse Icons

Repository for Fediverse icons

## Description
This is a convenient source for icons for the fediverse which I 
originally used for my blog as navigation icons.

## Usage
These are all SVG icons for various fediverse projects that I've found
in various places. See below for source information. They are not all 
of a standardized size so specifying the size specifically may be helpful to you.

### Monochrome 
<table>
    <tr>
        <td align="center">
            <a href="https://diasporafoundation.org/">
                <img width="50px" src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/diaspora.svg" alt="Diaspora Icon" />
                <br> Diaspora
            </a>
        </td>
        <td align="center">
            <a href="https://libreserver.org/epicyon/">
                <img width="50px" src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/epicyon.svg" alt="Epicyon Icon" />
                <br> Epicyon
            </a>
        </td>
        <td align="center>
    <a href=" https://friendi.ca/ ">
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/friendica.svg " alt="Friendica Icon "/>
        <br> Friendica</a>
   </td>
   <td align="center ">
    <a href="https://funkwhale.audio ">
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/funkwhale.svg " alt="Funkwhale Icon "/>
        <br> Funkwhale
    </a>
   </td>
   <td align="center ">
    <a href="https://gnusocial.network/ ">
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/gnu-social.svg " alt="GNU Social Icon ">
        <br> GNU Social
    </a>
   </td>
   <td align="center ">
    <a href="https://hubzilla.org ">
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/hubzilla.svg " alt="Hubzilla Icon ">
        <br> Hubzilla
    </a>
   </td>
   <td align="center ">
    <a href="https://joinmastodon.org "/>
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/mastodon.svg " alt = "Mastodon Icon ">
        <br> Mastodon
    </a>
   </td>
 </tr>
 <tr>
    <td align="center ">
    <a href="https://misskey-hub.net/ ">
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/misskey.svg " alt = "Misskey Icon ">
        <br> Misskey
    </a>   
   </td>
   <td align="center ">
    <a href="https://joinpeertube.org/ ">
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/peertube.svg " alt = "PeerTube Icon ">
        <br> PeerTube
    </a>   
   </td>
   <td align="center ">
    <a href="https://pixelfed.org/ ">
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/pixelfed.svg " alt = "Pixelfed Icon ">
        <br> Pixelfed
    </a>
   </td>
   <td align="center ">
    <a href="https://pleroma.social/ ">
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/pleroma.svg " alt = "Pleroma Icon ">
        <br> Pleroma
    </a>
   </td>  
   <td align="center ">
    <a href="https://socialhome.network/ ">
        <img width="50px " src="https://gitlab.com/HankG/fediverse-icons/-/raw/main/monochrome-icons/socialhome.svg " alt = "Socialhome Icon ">
        <br> Socialhome
    </a>   
   </td>            
 </tr>
<table>

## Support and Contributing
Please feel free to add issues to the issue tracker on this project if 
you see an error or want to contribute additional icons. Also feel free to reach
out to me on the fediverse [@hankg@friendica.myportal.social ](https://friendica.myportal.social/profile/hankg/profile).

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

[Jan Boddez's](https://geekcompass.com/@jan) [WordPress Fediverse Icons Plugin Repository](https://github.com/janboddez/add-fediverse-icons-to-jetpack) was the source of icons for:
 * Mastodon
 * PixelFed
 * Peertube
 * GNU Social
 * Friendica
 * Diaspora

[The Misskey main repository](https://github.com/joinmisskey/mk-assets/tree/master/icons) is the source of the Misskey icon.

[The SVG Repo](https://www.svgrepo.com/svg/362219/hubzilla) is the source fo the Hubzilla icon.

[The Socialhome documentation wiki](https://socialhome.readthedocs.io/en/latest/brand.html) is the source of the Socialhome icon.

[Wikipedia](https://www.wikipedia.org/) is the source of icons for:
 * [Funkwhale](https://commons.wikimedia.org/wiki/Category:Funkwhale_logos)
 * [Pleroma](https://commons.wikimedia.org/wiki/File:Pleroma_logo.svg)

## License
Creative Commons Attribution 4.0 International

